# PSI2 - Vícevláknový TCP server

Task was to create Multithread HTTP/TCP server that would send data over http protocol GET method and could service multiple clients.

# BUILD
All the neccesary code is written in main.py script run by command
```bash
python main.py
```
or
```bash
python3 main.py
```
depending on your operating system and version of python.

# SERVER DESCRIPTION
Server is listening on
```bash
localhost:8000
```
It is also a path to data for the main page Where you will see something like this:

<img src="img/mainpage.jpg">

There is also a link to subpage with another picture and link to go back to the main page.

<img src="img/secondpage.jpg">

Threads are created and managed by python library 
```bash
threading
```
and server statically creates 100 threads that listens to new or established connections through sockets. Each socket is reusable if not in use.

Server does not send .html files directly, but sends html content bytes directly.

