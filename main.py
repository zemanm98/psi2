import socket
import threading
import time
from http.server import BaseHTTPRequestHandler, HTTPServer


class Handler(BaseHTTPRequestHandler):

    def do_GET(self):
        #hlavni stranka
        if self.path == '/':
            self.send_response(200)
            self.send_header('Content-type', 'text/html; charset=utf-8')
            self.end_headers()
            self.wfile.write(bytes("<html><head><title>PSI2 python web server s vice vlaknama.</title></head></html>",
                                   'utf-8'))
            self.wfile.write(bytes("<body><h1>Nejaky hezky obrazek</h1><p>" +
                                   "<img src='7316.jpg' alt='img.jpg' width='320'></p>" +
                                   "<p><a href='good'>odkaz na dalsi stranku</a></p></body>", 'utf-8'))
        #odeslani dat o obrazku z hlavni stranky
        elif self.path == '/7316.jpg':
            self.send_response(200)
            self.send_header('Content-type', 'image/jpg')
            self.end_headers()
            f = open("img/7316.jpg", 'rb')
            self.wfile.write(f.read())
        # proklik na podstranku hlavni stranky pres odkdaz
        elif self.path == '/good':
            self.send_response(200)
            self.send_header('Content-type', 'text/html; charset=utf-8')
            self.end_headers()
            self.wfile.write(bytes("<html><head><title>PSI2 python web server s vice vlaknama.</title></head></html>",
                                   'utf-8'))
            self.wfile.write(bytes("<body><h1>Hezky obrazek lesa</h1><p>" +
                                   "<img src='toogood.jpg' alt='img.jpg' width='320'></p>" +
                                   "<p><a href='/'>Zpět</a></p></body>", 'utf-8'))
        # odeslani dat o obrazku na podstrance
        elif self.path == '/toogood.jpg':
            self.send_response(200)
            self.send_header('Content-type', 'image/jpg')
            self.end_headers()
            f = open("img/toogood.jpg", 'rb')
            self.wfile.write(f.read())
        else:
            self.send_error(404, "Object not found")

# server bude poslouchat na localhostu na portu 8000
addr = ('127.0.0.1', 8000)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(addr)
sock.listen(5)


# Spusti 100 posluchacskych vlaken
class Thread(threading.Thread):
    def __init__(self, i):
        threading.Thread.__init__(self)
        self.i = i
        self.daemon = True
        self.start()

    def run(self):
        httpd = HTTPServer(addr, Handler, False)
        httpd.socket = sock
        httpd.server_bind = self.server_close = lambda self: None
        httpd.serve_forever()
[Thread(i) for i in range(100)]
time.sleep(4000000)
